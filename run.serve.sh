#!/bin/bash

set -euo pipefail

function serve() {
  source __install.pnpm.sh

  pnpm install --no-frozen-lockfile
  pnpm run serve
}

if [[ -n ${SKIP_DOCKER:-} ]]; then
  echo "> not running inside of docker"
  if [[ $(uname) == "Darwin" ]] && [[ $(uname -m) == "arm64" ]]; then
    echo "> running in x86_64 shell '$0'"
    export SKIP_DOCKER
    arch -x86_64 bash -c "$0"
  else
    serve
  fi
elif [[ -f /.dockerenv ]]; then
  serve
else
  source __docker.sh
  run_docker ./run.serve.sh
fi
