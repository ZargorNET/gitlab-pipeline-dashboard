import axios, {AxiosInstance, AxiosRequestConfig} from 'axios'
import {
    GitLabBranch,
    GitLabGroup,
    GitLabPipelineDetails,
    GitLabPipelineJobs,
    GitLabPipelineSchedule,
    GitLabPipelineScheduleDetails,
    GitLabProject
} from '@/interfaces'


class GitLab {
    public client: AxiosInstance
    public apiBase = '/api/v4'

    constructor(url: string, token: string, clientConfig?: AxiosRequestConfig) {
        this.client = axios.create({
            baseURL: url,
            headers: {
                'Private-Token': token,
            },
            timeout: 10 * 1000,
            ...clientConfig,
        })
    }

    public searchGroups(search: string, options?: any): Promise<GitLabGroup[]> {
        return this.get('/groups', {search, ...options})
    }

    public searchProjects(search: string, options?: any): Promise<GitLabProject[]> {
        const archived: boolean = false
        return this.get(`/projects`, {search, archived, ...options})
            .then((response: GitLabProject[]) => {
                return response.filter(r => r.path_with_namespace === search)
            })
    }

    public async getAllProjects(groupId: number, options?: any): Promise<GitLabProject[]> {
        let projects = [] as GitLabProject[]
        let page = 1
        const perPage = 100
        while (page === 1 || projects.length === perPage) {
            const result = await this.get(`/groups/${groupId}/projects`, {
                archived: false,
                ...options,
                per_page: perPage,
                page,
            })
            projects = projects.concat(result)
            page++
        }
        return projects
    }

    public async getAllPipelines(projectId: number, ref?: string, onlyLatest?: boolean): Promise<GitLabPipelineDetails[]> {
        const params = ref ? {ref: ref} : {};
        // the /pipelines endpoint returns a more "shallow" struct i.e. it is missing the "started_at" property
        const shallowPipelines: Array<GitLabPipelineDetails> = await this.get(`/projects/${projectId}/pipelines`, params);

        let detailedPipelines: GitLabPipelineDetails[];
        if (onlyLatest)
            detailedPipelines = [await this.getPipeline(projectId, shallowPipelines[0].id)];
        else
            detailedPipelines = await Promise.all(shallowPipelines.map(pipeline => this.getPipeline(projectId, pipeline.id)));

        return detailedPipelines.length > 0 ? Promise.resolve(detailedPipelines) : Promise.reject("no pipelines found");
    }

    public getPipeline(projectId: number, pipelineId: number): Promise<GitLabPipelineDetails> {
        return this.get(`/projects/${projectId}/pipelines/${pipelineId}`)
    }

    public getPipelineSchedules(projectId: number): Promise<GitLabPipelineScheduleDetails[]> {
        const params = {
            scope: "active"
        };
        return this.get(`/projects/${projectId}/pipeline_schedules`, params)
            .then(
                schedules => Promise.all(schedules.map((schedule: GitLabPipelineSchedule) => {
                    return this.get(`/projects/${projectId}/pipeline_schedules/${schedule.id}`, params) as Promise<GitLabPipelineScheduleDetails>
                }))
            )
    }

    // see https://docs.gitlab.com/ee/api/jobs.html#list-pipeline-jobs
    public getPipelineJobs(projectId: number, pipelineId: number, scopes: string[]): Promise<GitLabPipelineJobs[]> {
        const params = {
            include_retries: true,
            scope: scopes
        };
        return this.get(`/projects/${projectId}/pipelines/${pipelineId}/jobs`, params)
    }

    public getBranches(project_id: number): Promise<GitLabBranch[]> {
        return this.get(`/projects/${project_id}/repository/branches`)
    }

    private async get(apiPath: string, params?: any) {
        const url = this.apiBase + apiPath
        const {data} = await this.client.get(url, {params})
        return data
    }
}

export default GitLab
