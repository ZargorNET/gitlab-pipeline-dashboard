function is_christmas_snowtime(): boolean {
    const date = new Date()
    return date.getMonth() == 11
}

export default is_christmas_snowtime
