import {GitLabPipelineDetails} from "@/interfaces/gitlab";

export interface Pipeline {
    projectName: string
    namespace: string
    details: GitLabPipelineDetails

    hint: string
    hint_url: string

    history: GitLabPipelineDetails[]
}
