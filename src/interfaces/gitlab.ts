export interface GitLabGroup {
    id: number
    description?: any
    default_branch: string
    ssh_url_to_repo: string
    http_url_to_repo: string
    web_url: string
    readme_url: string
    tag_list: string[]
    name: string
    name_with_namespace: string
    path: string
    path_with_namespace: string
    created_at: Date
    last_activity_at: Date
    forks_count: number
    avatar_url: string
    star_count: number
}

export interface GitLabProject {
    id: number
    description: string
    default_branch: string
    tag_list: any[]
    archived: boolean
    visibility: string
    ssh_url_to_repo: string
    http_url_to_repo: string
    web_url: string
    name: string
    name_with_namespace: string
    path: string
    path_with_namespace: string
    issues_enabled: boolean
    merge_requests_enabled: boolean
    wiki_enabled: boolean
    jobs_enabled: boolean
    snippets_enabled: boolean
    created_at: Date
    last_activity_at: Date
    shared_runners_enabled: boolean
    creator_id: number
    namespace: {
        id: number
        name: string
        path: string
        full_path: string
        kind: string
    }
    avatar_url?: any
    star_count: number
    forks_count: number
    open_issues_count: number
    public_jobs: boolean
    shared_with_groups: any[]
    request_access_enabled: boolean
}
export interface GitLabPipelineDetails {
    id: number
    sha: string
    ref: string
    status: string
    before_sha: string
    tag: boolean
    yaml_errors?: any
    user: {
        name: string
        username: string
        id: number
        state: string
        avatar_url: string
        web_url: string
    }
    created_at: Date
    updated_at: Date
    started_at?: any
    finished_at?: any
    committed_at?: any
    duration?: any
    coverage?: any
    web_url: string
    project_id: number
}

// see https://docs.gitlab.com/ee/api/jobs.html#get-a-single-job
export interface GitLabPipelineJobs {
    id: number
    status: string
}

export interface GitLabBranch {
    name: string
    protected: boolean
}

// see https://docs.gitlab.com/ee/api/pipeline_schedules.html#get-all-pipeline-schedules
export interface GitLabPipelineSchedule {
    id: number
    description: string
}

// see https://docs.gitlab.com/ee/api/pipeline_schedules.html#get-a-single-pipeline-schedule
export interface GitLabPipelineScheduleDetails {
    id: number
    description: string
    last_pipeline: {
        id: number
        status: string
    }
}
