const {defineConfig} = require("@vue/cli-service");

// see https://cli.vuejs.org/config/#publicpath
module.exports = defineConfig({
    publicPath: process.env.NODE_ENV === 'production'
        ? process.env.PUBLIC_PATH ? process.env.PUBLIC_PATH : '/public_path_not_found'
        : '/',
    transpileDependencies: true
});

const git = require('git-rev-sync');
process.env.VUE_APP_GIT_DATE = git.date()
process.env.VUE_APP_GIT_HASH = git.long()
