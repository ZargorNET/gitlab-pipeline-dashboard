#!/bin/bash


export PATH=$PWD/node_modules/.bin:$PATH

pnpm_version='7.18.2'
echo
echo "> check 'pnpm' ($pnpm_version) is installed ... "

if ! command -v pnpm >/dev/null || [[ $(pnpm --version) != "$pnpm_version" ]]; then
  npm install --no-save pnpm@"$pnpm_version"
fi

echo "-----------------------------------------------------"
echo
